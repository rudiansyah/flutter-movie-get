import 'package:flutter/material.dart';
import 'package:movie_get/services/movie_services.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Naruto Movie"),
        backgroundColor: Colors.yellow,
      ),
      body: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            builder: (context) => MovieServices(),
          )
        ],
        child: SingleChildScrollView(
          child: HomeBody()
        ),
      )
    );
  }
}

class HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final movieProvider = Provider.of<MovieServices>(context);
    if(movieProvider.getMovieList == null) {
      movieProvider.fetchMovie();
    }

    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Text("Daftar Movie",
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black87,
                  fontWeight: FontWeight.bold
                ,)
              ),

              movieProvider.getMovieList != null ? ListMovie() : 
              Center(
                child: CircularProgressIndicator(),
              )
        ],
      ),
    );
  }
}


class ListMovie extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final movieProvider = Provider.of<MovieServices>(context);
    return Container(
      child: ListView.builder(
        itemCount: movieProvider.getMovieList.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, int index) {
          return ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 64,
                    height: 64,
                    margin: EdgeInsets.only(right: 10),
                    child: Image.network(movieProvider.getMovieList[index].imageUrl, fit: BoxFit.cover)
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width / 1.7,
                        child: Text(
                          movieProvider.getMovieList[index].title,
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.black87
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      
                      Container(
                        width: MediaQuery.of(context).size.width / 1.7,
                        child: Text(
                          movieProvider.getMovieList[index].synopsis,
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black45
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                      ),
                       )
                    ],
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}