
class MovieModel {
  String title;
  String imageUrl;
  String synopsis;
  // MovieModel Consctructor
  MovieModel({
    this.title, this.imageUrl, this.synopsis
  });

  factory MovieModel.fromJson(Map<String, dynamic> json) {
    return MovieModel(
      title: json["title"],
      imageUrl: json["image_url"],
      synopsis: json["synopsis"]
    );
  }
}