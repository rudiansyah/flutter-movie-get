import 'package:flutter/material.dart';
import 'package:movie_get/model/movie_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class MovieServices extends ChangeNotifier {
  List<MovieModel> movieList;
  List<MovieModel> get getMovieList => movieList;

  Future<void> fetchMovie() async {
    String url = "https://api.jikan.moe/v3/search/anime?q=naruto&limit=15";
    var response = await http.get(url, headers: {
      "Accept": 'application/json'
    });

    if(response.statusCode == 200) {
      movieList = new List<MovieModel>();
      var data = json.decode(response.body);
      for(int i=0; i<data["results"].length; i++) {
        movieList.add(
          MovieModel.fromJson(data["results"][i])
        );
      }
      notifyListeners();
    }
  } 
}